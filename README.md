# cooltano

reduced testcases for https://gitlab.com/meltano/meltano

## docker

```bash
./cooltano.py build cooltano/tap-github git+https://github.com/singer-io/tap-github.git
./cooltano.py build cooltano/target-csv target-csv


# make a config.json
# {"access_token": "your-access-token", "repository": "singer-io/tap-github"}

./cooltano.py run cooltano/tap-github tap-github -c config.json --discover > properties.json

cp properties.json properties-selected.json

# select some properties

./cooltano.py run cooltano/tap-github tap-github -c config.json --properties properties-selected.json | ./cooltano.py run cooltano/target-csv target-csv

```