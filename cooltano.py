#!/usr/bin/env python
import os
from subprocess import Popen, PIPE
import sys
from uuid import uuid4


def build(name, remote):
    p = Popen([
        'docker',
        'build',
        '-t', name,
        '--build-arg', f'remote={remote}',
        'docker/singer',
    ])
    p.wait()


def run(name, *command):
    workdir = '/usr/src/app'
    p = Popen([
        'docker', 'run',
        '-i', '--rm',
        '--name', f'cool-{uuid4()}',
        '-v', f'{os.getcwd()}:{workdir}',
        '-w', workdir,
        name,
        *command
    ], stdin=sys.stdin, bufsize=1)

    p.wait()


def usage(cmd):
    print('Usage:')
    print(f'{cmd} build NAME REMOTE')
    print(f'{cmd} run NAME COMMAND')


def main(args):
    if len(args) < 2:
        usage(args[0])
        sys.exit(0)

    if args[1] == 'build':
        if len(args) < 4:
            usage(args[0])
            sys.exit(1)

        return build(args[2], args[3])

    if args[1] == 'run':
        if len(args) < 3:
            usage(args[0])
            sys.exit(1)

        return run(args[2], *args[3:])


if __name__ == '__main__':
    main(sys.argv)
